# Flex Pi setup script

## Setup script for Flex Pi.

Sådan gør du:
* Hent nyeste raspbian lite https://downloads.raspberrypi.org/raspbian_lite_latest
* Flash til mikro-SD med eksempelvis Etcher https://www.balena.io/etcher/
* Sæt mikro-SD-kortet i Pi'en
* Saml Pi'en
* Forbind den til en skærm, sæt keyboard, netværkskabel, og strøm til
* Log ind med:
    * Bruger: pi
    * Password: raspberry 
* Hvis keyboardet ikke er korrekt, se sektionen Keyboard
    * Ikke nødvendigt, men gør det lettere at skrive følgende kommando
* `curl -L https://gitlab.com/Philliarch/pftsscript/raw/master/setup.sh | bash`
* Skift password for brugeren pi med passwd (CTRL+ALT+T for at åbne terminalen)

## Resources:
* curl to bash: https://linuxhint.com/curl_bash_examples/
* sed: https://www.cyberciti.biz/faq/how-to-use-sed-to-find-and-replace-text-in-files-in-linux-unix-shell/
* fancy colours in terminal: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
* man for apt: http://manpages.ubuntu.com/manpages/xenial/man8/apt.8.html
* bash script condition: https://www.shellhacks.com/check-if-string-exists/
* add cronjob one-liner: https://stackoverflow.com/questions/4880290/how-do-i-create-a-crontab-through-a-script

## Keyboard

### For hurtigste skift til dansk keyboard, kør: 

* `sudo update-locale en_DK.UTF-8`
* `sudo locale-gen`
* `sudo sed -i 's/"gb"/"dk"/g' /etc/default/keyboard`
* `reboot`

### For mest simple skift til dansk keyboard og locale

* Kør `sudo dpkg-reconfigure locales`
* Find en_DK.UTF-8 i listen og vælg med mellemrum. Afslut med enter.
* Vælg en_DK.UTF-8 som default locale. Systemet genererer nu den nødvendige data.
* Genstart; kør `reboot`
* Kør `sudo dpkg-reconfigure keyboard-configuration`
* Vælg Generic 105-key (...)
* Vælg Other
* Vælg Danish
* For resten af denne dialog-serie: vælg øverste valgmulighed
* Genstart; kør `reboot`

## Actions:
* write progress messages
* update system
* install desktop environment
* install unclutter+chromium
* make directory for ~/.config/lxsession/LXDE-pi/autostart
* replace ~/-config/lxsession/LXDE-pi/autostart-file
* conditional insert of line for lcd 180 deg. rotation
* replace line for autologin
* add autoupdater cronjob
* reboot
