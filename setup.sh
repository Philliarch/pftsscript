#!/bin/bash

COLOR='\033[0;32m' # dejlig grøn
ENDCOLOR='\033[1;37m' # hvid
# ${COLOR} før ønsket tekst for at anvende på resten af linjen
# Progress-beskeder echoes. -e i echo betyder at den skal tage specialtegn, eks. \n som linjeskift.
echo -e "${COLOR}Flexterminal-setup starter\nOpdaterer system${ENDCOLOR}"
# sleep i 1 sekund for synlighed af progress
sleep 1s
# Opdater system
sudo apt update
sudo apt full-upgrade -y

# Installation af Unclutter til at usynliggøre musemarkøren, Chromium-browseren, og desktop-manager
echo -e "${COLOR}Installerer desktop, unclutter, og chromium-browser${ENDCOLOR}"
sleep 1s
sudo apt install --no-install-recommends xserver-xorg xinit -y
sudo apt install raspberrypi-ui-mods -y
sudo apt install unclutter chromium-browser -y

# Oprettelse af ~/.config/lxsession/LXDE-pi/ til brug til at starte unclutter og Chromium
echo -e "${COLOR}Opretter ~/.config/lxsession/LXDE-pi/ til autostart hvis mappen ikke eksisterer${ENDCOLOR}"
sleep 1s
# Argument -p for at oprette hele stien (~/.config/lxsession/ mangler som standard)
mkdir -p ~/.config/lxsession/LXDE-pi/

# Indsætning af programmer der skal starte i ~/.config/lxsession/LXDE-pi/autostart-filen med konfiguration 
# VHA >> (output redirection), som her bruges til at indsætte output fra echo-kommandoen i en fil i stedet for terminalen
echo -e "${COLOR}Erstatter indholdet af autostart-filen til autostart af unclutter, og Chromium, og at slå screensaver fra${ENDCOLOR}"
sleep 1s
echo -e "@unclutter -display:0 -noevents -grab\n@xset s off\n@xset -dpms\n@chromium-browser --kiosk --incognito https://flextid.it/pi" > ~/.config/lxsession/LXDE-pi/autostart

# NB! sudo bash -c '$commands' bruges til at gøre det muligt at bruge output redirection (>>) til at indsætte en linje i en fil som kræver
# flere permissions end den pågældende bruger har
sleep 1s
# Her indsættes 180 graders rotation af display
# Conditional for ikke at indsætte samme linje flere gange
if grep -q 'lcd_rotate=2' '/boot/config.txt';
    then
        echo -e "${COLOR}lcd_rotate=2 eksisterer allerede i /boot/config.txt${ENDCOLOR}"
    else
        echo -e "${COLOR}Indsætter 180 graders rotation af display for korrekt visning${ENDCOLOR}"
        sudo bash -c 'echo "lcd_rotate=2" >> /boot/config.txt'
fi

# Her slås touch fra
# Conditional for ikke at indsætte samme linje flere gange
if grep -q 'disable_touchscreen=1' '/boot/config.txt';
    then
        echo -e "${COLOR}disable_touchscreen=1 eksisterer allerede i /boot/config.txt${ENDCOLOR}"
    else
        echo -e "${COLOR}Indsætter 180 graders rotation af display for korrekt visning${ENDCOLOR}"
        sudo bash -c 'echo "disable_touchscreen=1" >> /boot/config.txt'
fi

echo -e "${COLOR}Autologin til brugeren pi${ENDCOLOR}"
sleep 1s
# Sed bruges her til at erstatte linjen "#autologin-user=" med "autologin-user=pi". 
# -i betyder at noget tekst skal indsættes. s/ betyder find mønster/tekst, efter næste / er det der skal erstattes med, /g definerer at det fundne skal erstattes.
sudo sed -i 's/#autologin-user=/autologin-user=pi/g' /etc/lightdm/lightdm.conf
sleep 1s
echo -e "${COLOR}Genstarter${ENDCOLOR}"
# Afslut med at genstarte
sudo reboot
